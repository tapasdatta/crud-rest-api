<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

use App\User;
use App\Place;
use App\Checkin;
class PlaceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        for($i = 0; $i < 100; $i++) {
        	DB::table('places')->insert([
                'id'         => Uuid::generate(4),
        		'name'       => $faker->name,
                'lat'        => $faker->latitude,
                'lon'        => $faker->longitude,
                'address1'   => $faker->streetAddress,
                'address2'	 => $faker->country,
                'city'       => $faker->city,
                'state'      => $faker->stateAbbr,
                'zip'        => rand(10000, 30000),
                'website'    => $faker->url,
                'phone'      => $faker->phoneNumber,
        	]);	

        }
    }
}
