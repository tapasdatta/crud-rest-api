<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        for($i = 0; $i < 100; $i++) {
        	DB::table('users')->insert([
                'id'         => Uuid::generate(4),
        		'name' 		=>	$faker->name,
        		'email' 	=>	$faker->email,
        		'active'	=>	$i === 0 ? true : rand(0, 1),
        		'gender'	=>	rand(0, 1) ? 'm' : 'f',
        		'birthday'	=>	$faker->dateTimeBetween('-40 years', '-18 years'),
        		'location'	=> "{$faker->city}, {$faker->state}",
        		'bio'		=>	$faker->sentence(100)
        	]);
        }
    }
}
