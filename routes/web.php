<?php

Route::group(['prefix' => 'api/v1'], function () {
	
	//create users(not idempotent)
	Route::post('/users', 'UsersController@create');

	//get users by id
	Route::get('/users/{id}', "UsersController@show");

	//update users by id(idempotent)
	Route::put('/users/{id}', "UsersController@update");

	//delete users by id(idempotent)
	Route::delete('/users/{id}', 'UsersController@delete');

	//get list of users
	Route::get('/users', 'UsersController@list');

	//upload images of given user id(idempotent)
	Route::put('/users/{id}/image', "UsersController@uploadImage");

	//get users faviourites by id
	Route::get('/users/{id}/favourites', 'UsersController@favourites');

	//get users checkins by id
	Route::get('/users/{id}/checkins', 'UsersController@checkins');




	//create places(not idempotent)
	Route::post('/places', 'PlacesController@create');

	//get places by id
	Route::get('/places/{id}', "PlacesController@show");

	//update places by id(idempotent)
	Route::put('/places/{id}', "PlacesController@update");

	//delete places by id(idempotent)
	Route::delete('/places/{id}', 'PlacesController@delete');

	//get list of places
	Route::get('/places', 'PlacesController@list');

	//upload images of given place id(idempotent)
	Route::put('/places/{id}/image', "PlacesController@uploadImage");

	//get places faviourites by id
	Route::get('/places/{id}/favourites', 'PlacesController@favourites');

	//get places checkins by id
	Route::get('/places/{id}/checkins', 'PlacesController@checkins');
	
});


