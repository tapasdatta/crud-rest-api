<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Place extends Model
{

    public function checkins()
    {
    	return $this->hasMany('Checkin');
    }
}
