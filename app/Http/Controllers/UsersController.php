<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\ApiController as API;
use App\Transformer\UserTransformer;
use Illuminate\Http\Request;

class UsersController extends API
{


	public function show($id)
	{
		$user = User::find($id);

		return $this->respondWithItem($user, new UserTransformer);
	}


	public function list()
	{
		$users = User::take(10)->get();

		return $this->respondWithCollection($users, new UserTransformer);
	}

}
