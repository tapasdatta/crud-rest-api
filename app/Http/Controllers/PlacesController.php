<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Place;
use App\Http\Controllers\ApiController as API;
use App\Transformer\PlaceTransformer;

class PlacesController extends API
{

  

	public function show($id)
	{
		$place = Place::find($id);

		return $this->respondWithItem($place, new PlaceTransformer);
	}

	public function list()
	{
		$places = Place::take(10)->get();

		return $this->respondWithCollection($places, new PlaceTransformer);
	}

}
