<?php
namespace App\Transformer;
use League\Fractal\TransformerAbstract;
use App\User;

class UserTransformer extends TransformerAbstract
{
	public function transform(User $user)
	{
		return [
			'name'		=>	$user->name,
			'email'		=>	$user->email,
			'active'	=>	$user->active,
			'gender'	=>	$user->gender,
			'location'	=>	$user->location
		];
	}
}