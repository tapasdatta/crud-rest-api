<?php

namespace App\Transformer;
use App\Place;
use League\Fractal\TransformerAbstract;


class PlaceTransformer extends TransformerAbstract
{
	public function transform(Place $place)
	{
		return [
            'id'         => (int) $place->id,
            'name'       => $place->name,
            'lat'        => (float) $place->lat,
            'lon'        => (float) $place->lon,
            'address1'	 => (string) $place->address1,
            'address2'	 => (string) $place->address2,
            'city'       =>	(string) $place->city,
            'state'	 => (string) $place->state,
            'zip'		 => (string) $place->zip,
            'website'	 => $place->website,
            'phone'		 => $place->phone,
            'created_at' => (string) $place->created_at,
		];
	}
}