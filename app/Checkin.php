<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Checkin extends Model
{
    public function user()
    {
    	return $this->belongsTo('User');
    }

    public function place()
    {
    	return $this->belongsTo('Place');
    }
}
